-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 08-Out-2020 às 00:49
-- Versão do servidor: 10.4.14-MariaDB
-- versão do PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `caomigo`
--
CREATE DATABASE `caomigo`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `adocao`
--

CREATE TABLE `adocao` (
  `id_adocao` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id_animal` int(11) NOT NULL,
  `id_processo` int(11) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `animal`
--

CREATE TABLE `animal` (
  `id_animal` int(11) NOT NULL,
  `id_processo` int(11) NOT NULL,
  `nome_animal` varchar(45) NOT NULL,
  `raca` varchar(45) NOT NULL,
  `idade` varchar(45) DEFAULT NULL,
  `porte` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `cor` varchar(55) DEFAULT NULL,
  `foto` varchar(600) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `animal`
--

INSERT INTO `animal` (`id_animal`, `id_processo`, `nome_animal`, `raca`, `idade`, `porte`, `descricao`, `cor`, `foto`) VALUES
(12, 2, 'kiko', 'boxer', 'branco', '2 anos', 'grande', 'null', 'image6.jpg'),
(13, 3, 'Juju', 'vira-lata', 'marrom', '1 ano', 'medio', 'null', 'dog6.jpg'),
(18, 1, 'Lele', 'vira-lata', 'Branco e Preto', '1.5 anos', 'Grande', 'Bravo', 'image10.jpg'),
(19, 5, 'Luke', 'Buldog Fancês', 'Fulvo', '3 anos', 'Médio', 'Doidinho', 'luke.jpg'),
(20, 1, 'ss', 'vira-lata', 'Branco e Preto', '1.5 anos', 'Médio', 'Doidinho', 'image12.jpg'),
(21, 1, 'Lele', 'vira-lata', 'Bege', '2 anos', 'Médio', 'Brincalhão', 'all-bg-title.jpg'),
(22, 1, 'Niina', 'Vira-lata', 'Bege', '3 meses', 'Pequeno', 'Quieta', 'dog4.jpg'),
(23, 1, 'Antonio', 'Schnauzer', 'Preto', '10 anos', 'Médio', 'Tranquilo', 'dog7.jpg'),
(24, 1, 'Xua', 'Chihuahua', 'Bege e Preto', '9 Meses', 'Pequeno', 'Brincalhão', 'dog8.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `processo`
--

CREATE TABLE `processo` (
  `id_processo` int(11) NOT NULL,
  `processo` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `processo`
--

INSERT INTO `processo` (`id_processo`, `processo`) VALUES
(1, 'Para Adoção'),
(2, 'Encontrou uma fámilia'),
(3, 'Vacinado'),
(4, 'Castrado'),
(5, 'Adotado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(64) NOT NULL,
  `id_level_user` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cidade` varchar(45) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `cep` int(9) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`user_id`, `email`, `senha`, `id_level_user`, `user_name`, `cpf`, `endereco`, `bairro`, `cidade`, `uf`, `cep`, `telefone`, `foto`) VALUES
(1, 'isabellaveras98@hotmail.com', 'UCjKzMPCSNiSkRXQF4hf7kzZK5IoCw4l', 1, 'Isabella Veras Alves Souza', '59455357020', 'Rua Pequenina', 'Pequeno', 'Cidade Grande', 'SP', 3521478, '11987536421', 'img-2.jpg'),
(15, 'paula@gmail.com', '123546', 2, 'Salete', '14788569324', 'Osiris Magalhães de Almeida', 'Monte Kemel', 'São Paulo', 'SP', 3521478, '11365475893', ''),
(22, 'leonardo@gmail.com', '1234', 2, 'Leonardo Souza', '57498712536', 'Rua Ruinha', 'Bairrozinho', 'Cidadezinha', 'MG', 65874103, '33698741054', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_level`
--

CREATE TABLE `user_level` (
  `id_level_user` int(11) NOT NULL,
  `level_user` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `user_level`
--

INSERT INTO `user_level` (`id_level_user`, `level_user`) VALUES
(1, 'administrador'),
(2, 'usuario');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `adocao`
--
ALTER TABLE `adocao`
  ADD PRIMARY KEY (`id_adocao`),
  ADD KEY `id_animal` (`id_animal`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `id_processo` (`id_processo`);

--
-- Índices para tabela `animal`
--
ALTER TABLE `animal`
  ADD PRIMARY KEY (`id_animal`),
  ADD KEY `id_processo` (`id_processo`);

--
-- Índices para tabela `processo`
--
ALTER TABLE `processo`
  ADD PRIMARY KEY (`id_processo`);

--
-- Índices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `id_level_user` (`id_level_user`);

--
-- Índices para tabela `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id_level_user`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `adocao`
--
ALTER TABLE `adocao`
  MODIFY `id_adocao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `animal`
--
ALTER TABLE `animal`
  MODIFY `id_animal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de tabela `processo`
--
ALTER TABLE `processo`
  MODIFY `id_processo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id_level_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `adocao`
--
ALTER TABLE `adocao`
  ADD CONSTRAINT `adocao_ibfk_1` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id_animal`),
  ADD CONSTRAINT `adocao_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `adocao_ibfk_3` FOREIGN KEY (`id_processo`) REFERENCES `processo` (`id_processo`);

--
-- Limitadores para a tabela `animal`
--
ALTER TABLE `animal`
  ADD CONSTRAINT `animal_ibfk_1` FOREIGN KEY (`id_processo`) REFERENCES `processo` (`id_processo`);

--
-- Limitadores para a tabela `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level_user`) REFERENCES `user_level` (`id_level_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
