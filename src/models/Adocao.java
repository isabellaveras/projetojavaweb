package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import util.DBQuery;

public class Adocao {

	private int idAdocao;
	private int userId;
	private int idAnimal;
	private int idProcesso;
	private Date data;

	private String tableName  = "adocao";
	private String fieldsName = "id_adocao, user_id id_animal ,id_processo, data";
	private String fieldKey   = "id_adocao";

	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);

	public Adocao() {

	}


	public int getIdAdocao() {
		return idAdocao;
	}

	public void setIdAdocao(int idAdocao) {
		this.idAdocao = idAdocao;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getIdAnimal() {
		return idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}


	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";


		while (rs.next()) {
			saida += "<tr>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_adocao") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("user_id") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_animal") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("id_processo") + "</td>";
			saida += "<td class grid-tabels-td text-center>"+rs.getString("data") + "</td>";
			saida += "</tr>";
			System.out.println();
		}

		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	public void deleteaAdocao(String where) {
		String query =  "DELETE FROM adocao WHERE " + where;
		dbQuery.execute(query);
	}
	




}


