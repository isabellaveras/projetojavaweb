package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import util.DBQuery;

public class NivelUsuario {
	

	
	
	private int	idNivelUsuario;
	private String	nivelUsuario;
	
	
	private String tableName  = "user_level";
	private String fieldsName = "id_level_user, level_user";
	private String fieldKey   = "id_level_user";
	 
	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	 
	public NivelUsuario() {
		
	}
	
	public int getIdNivelUsuario() {
		return idNivelUsuario;
	}

	public void setIdNivelUsuario(int idNivelUsuario) {
		this.idNivelUsuario = idNivelUsuario;
	}

	public String getNivelUsuario() {
		return nivelUsuario;
	}

	public void setNivelUsuario(String nivelUsuario) {
		this.nivelUsuario = nivelUsuario;
	}
	
	public NivelUsuario(int idNivelUsuario, String nivelUsuario ) {
		this.setIdNivelUsuario(idNivelUsuario);
		this.setNivelUsuario(nivelUsuario);
	}

	private String[] toArray() {
		String[] data = new String[] {
		 Integer.toString(this.getIdNivelUsuario()),
		 this.getNivelUsuario()};
		return(data);

	}
	
	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";
		
		
			while (rs.next()) {
				saida += "<tr>";
				saida += "<td>"+rs.getString("id_level_user") + "</td>";
				saida += "<td>"+rs.getString("level_user") + "</td>";
				saida += "</tr>";
				System.out.println();
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	
}
