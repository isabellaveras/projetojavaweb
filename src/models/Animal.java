package models;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.DBQuery;

public class Animal {
	
	private int idAnimal;
	private int idProcesso;
	private String nomeAnimal;
	private String raca;
	private String idade;
	private String porte;
	private String descricao;
	private String cor;
	private String foto;
	
	private String tableName  = "animal";
	private String fieldsName = "id_animal, id_processo, nome_animal, raca, idade, porte, descricao, cor, foto";
	private String fieldKey   = "id_animal";
	
	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	
	public Animal() {

	}


	public Animal(int idAnimal, int idProcesso, String nomeAnimal, String raca, String idade, String porte,
			String descricao, String cor, String foto) {
		this.setIdAnimal(idAnimal);
		this.setIdProcesso(idProcesso);
		this.setNomeAnimal(nomeAnimal);
		this.setRaca(raca);
		this.setIdade(idade);
		this.setPorte(porte);
		this.setDescricao(descricao);
		this.setCor(cor);
		this.setFoto(foto);
	}
	
	private String[] toArray() {
		String[] data = new String[] {
		Integer.toString(this.getIdAnimal()),
		Integer.toString(this.getIdProcesso()),
		this.getNomeAnimal(),
		this.getRaca(),
		this.getCor(),
		this.getIdade(),
		this.getPorte(),
		this.getDescricao(),
		this.getFoto()};
		return(data);
	}



	public int getIdAnimal() {
		return idAnimal;
	}


	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}


	public int getIdProcesso() {
		return idProcesso;
	}


	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}


	public String getNomeAnimal() {
		return nomeAnimal;
	}


	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}


	public String getRaca() {
		return raca;
	}


	public void setRaca(String raca) {
		this.raca = raca;
	}


	public String getIdade() {
		return idade;
	}


	public void setIdade(String idade) {
		this.idade = idade;
	}


	public String getPorte() {
		return porte;
	}


	public void setPorte(String porte) {
		this.porte = porte;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public String getCor() {
		return cor;
	}


	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getFoto() {
		return foto;
	}
	
	
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public ResultSet selectOneById(String id) throws SQLException {
		ResultSet rs = this.dbQuery.select(fieldKey +"=" +id);
		
		
		return rs;

	}
	public void save() {
		if ( this.getIdAnimal() > 0 ) {
			this.dbQuery.update( this.toArray() );
		} else {
			this.dbQuery.insert( this.toArray() );
		}
	}

	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";
		saida +="<tr> <td class='grid-tabels-td text-center'>Id Animal</td><td class='grid-tabels-td text-center'>Id Processo</td><td class='grid-tabels-td text-center'>Nome</td>"
				+ "<td class='grid-tabels-td text-center'>Ra�a</td><td class='grid-tabels-td text-center'>Idade</td><td class='grid-tabels-td text-center'>Porte</td>"
				+ "<td class='grid-tabels-td text-center'>Descri��o</td><td class='grid-tabels-td text-center'>Cor</td><td class='grid-tabels-td text-center'>Foto</td> "
				+ "<td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";
		
		
			while (rs.next()) {
				
				saida += "<tr>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_animal") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_processo") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nome_animal") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("raca") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("idade") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("porte") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("descricao") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cor") + "</td>";
				saida += "<td class='grid-tabels-td text-center'><img src='images/"+rs.getString("foto") + " 'style=width:100px;></td>";
				saida += "<td class='grid-tabels-td text-center'><div class=update-box><button onclick=window.location.href='editar.jsp?tipo=1&id="+ rs.getString("id_animal")+"'>Editar</button></div></td>";
				saida += "<td class='grid-tabels-td text-center'><div class=update-box><button onclick=window.location.href='delecao.jsp?tipo=1&id="+ rs.getString("id_animal")+"'>Excluir</button></div></td>";
				saida += "</tr>";
				System.out.println();
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	public String listAnimaisProcesso(String where) throws SQLException {
		String query =  "SELECT * FROM animal WHERE id_processo =" + where;
		ResultSet rs = dbQuery.query(query);
		String saida = "<br>";
		saida += "<table border=1>";
		saida +="<tr> <td class='grid-tabels-td text-center'>Id Animal</td><td class='grid-tabels-td text-center'>Id Processo</td><td class='grid-tabels-td text-center'>Nome</td>"
				+ "<td class='grid-tabels-td text-center'>Ra�a</td><td class='grid-tabels-td text-center'>Idade</td><td class='grid-tabels-td text-center'>Porte</td>"
				+ "<td class='grid-tabels-td text-center'>Descri��o</td><td class='grid-tabels-td text-center'>Cor</td><td class='grid-tabels-td text-center'>Foto</td> "
				+ "<td class='grid-tabels-td text-center'>Editar</td><td class='grid-tabels-td text-center'>Excluir</td></tr>";
		
		
		
		
		
			while (rs.next()) {
				saida += "<tr>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_animal") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("id_processo") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("nome_animal") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("raca") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("idade") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("porte") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("descricao") + "</td>";
				saida += "<td class='grid-tabels-td text-center'>"+rs.getString("cor") + "</td>";
				saida += "<td class='grid-tabels-td text-center'><img src='images/"+rs.getString("foto") + " 'style=width:100px;></td>";
				saida += "<td class='grid-tabels-td text-center'><div class=update-box><button onclick=window.location.href='editar.jsp?tipo=1&id="+ rs.getString("id_animal")+"'>Editar</button></div></td>";
				saida += "<td class='grid-tabels-td text-center'><div class=update-box><button onclick=window.location.href='delecao.jsp?tipo=1&id="+ rs.getString("id_animal")+"'>Excluir</button></div></td>";
				saida += "</tr>";
				System.out.println();
				
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	public void deleteAnimal(String id) {
		dbQuery.delete(id);
		
	}
	
	public void insertAnimal(String [] values) {
		dbQuery.insert(values);
	}
	
	

}
