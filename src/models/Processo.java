package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import util.DBQuery;

public class Processo {
	

	
	
	private int	idProcesso;
	private String	processo;
	
	
	private String tableName  = "processo";
	private String fieldsName = "id_processo, processo";
	private String fieldKey   = "id_processo";
	 
	private DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	 
	public Processo() {
		
	}
	
	public int getidProcesso() {
		return idProcesso;
	}

	public void setidProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}

	public String getprocesso() {
		return processo;
	}

	public void setprocesso(String processo) {
		this.processo = processo;
	}
	
	public Processo(int idProcesso, String processo ) {
		this.setidProcesso(idProcesso);
		this.setprocesso(processo);
	}

	private String[] toArray() {
		String[] data = new String[] {
		 Integer.toString(this.getidProcesso()),
		 this.getprocesso()};
		return(data);

	}
	
	public String listAll() throws SQLException {
		ResultSet rs =  this.dbQuery.select("");
		String saida = "<br>";
		saida += "<table border=1>";
		
		
			while (rs.next()) {
				saida += "<tr>";
				saida += "<td class grid-tabels-td>"+rs.getString("id_processo") + "</td>";
				saida += "<td class grid-tabels-td>"+rs.getString("processo") + "</td>";
				saida += "</tr>";
				System.out.println();
			}
		
		rs.close();
		saida += "</table>";
		return (saida);
	}
	
	


}
