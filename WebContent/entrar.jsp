<%@page import="java.util.Base64"%>

<%@page import="models.Usuario"%>
<%@page import="services.Login"%>
<%

String login = request.getParameter("login");
String senha = request.getParameter("senha");

Login loginTest = new Login();
Usuario requestLogin = loginTest.getLogin(login, senha);


if(requestLogin == null)
{
	
	
	response.sendRedirect("login.jsp?erroLogin=1");
	
}
else
{
	
Cookie cookieLogin = new Cookie("login",login);
cookieLogin.setMaxAge(60*60*72);
response.addCookie(cookieLogin);
	
Cookie cookieSenha = new Cookie("senha",senha);
cookieSenha.setMaxAge(60*60*72); 
response.addCookie(cookieSenha);

String nivelUsuario =Integer.toString( requestLogin.getIdNivelUsuario());
Cookie nivelUsuarioCookie = new Cookie("idNivelUsuario", nivelUsuario);
nivelUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(nivelUsuarioCookie);

String nomeUsuario = requestLogin.getNome();
nomeUsuario = Base64.getEncoder().encodeToString(nomeUsuario.getBytes());
Cookie nomeUsuarioCookie = new Cookie("userName",nomeUsuario);
nomeUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(nomeUsuarioCookie);


String cpfUsuario = requestLogin.getCpf();
Cookie cpfUsuarioCookie = new Cookie("cpf",cpfUsuario);
cpfUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(cpfUsuarioCookie);


String enderecoUsuario = requestLogin.getEndereco();
enderecoUsuario = Base64.getEncoder().encodeToString(enderecoUsuario.getBytes());
Cookie enderecoUsuarioCookie = new Cookie("endereco",enderecoUsuario);
enderecoUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(enderecoUsuarioCookie);



String bairroUsuario = requestLogin.getBairro();
bairroUsuario = Base64.getEncoder().encodeToString(enderecoUsuario.getBytes());
Cookie bairroUsuarioCookie = new Cookie("bairro",bairroUsuario);
bairroUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(bairroUsuarioCookie);


String cidadeUsuario = requestLogin.getCidade();
cidadeUsuario = Base64.getEncoder().encodeToString(cidadeUsuario.getBytes());
Cookie cidadeUsuarioCookie = new Cookie("cidade",cidadeUsuario);
cidadeUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(cidadeUsuarioCookie);



String ufUsuario = requestLogin.getUf();
Cookie ufUsuarioCookie = new Cookie("uf",ufUsuario);
ufUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(ufUsuarioCookie);


String cepUsuario = requestLogin.getCep();
Cookie cepUsuarioCookie = new Cookie("cep",cepUsuario);
cepUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(cepUsuarioCookie);


String telefoneUsuario = requestLogin.getTelefone();
Cookie telefoneUsuarioCookie = new Cookie("telefone",telefoneUsuario);
telefoneUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(telefoneUsuarioCookie);

String fotoUsuario = requestLogin.getFoto();
Cookie fotoUsuarioCookie = new Cookie("foto",fotoUsuario);
fotoUsuarioCookie.setMaxAge(60*60*72);
response.addCookie(fotoUsuarioCookie);

response.sendRedirect("index.jsp");

}






























%>