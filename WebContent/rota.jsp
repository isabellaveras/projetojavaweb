<%@page import="models.Animal"%>
<%@page import="models.Usuario"%>

<%
		
	if (request.getParameter("idUsuario") != null) {
		int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		int idNivelUsuario = Integer.parseInt(request.getParameter("idNivelUsuario"));
		String nome = request.getParameter("nome");
		String cpf = request.getParameter("cpf");
		String endereco = request.getParameter("endereco");
		String bairro = request.getParameter("bairro");
		String cidade = request.getParameter("cidade");
		String uf = request.getParameter("uf");
		String cep = request.getParameter("cep");
		String telefone = request.getParameter("telefone");
		String foto = request.getParameter("foto");

		Usuario usuarioWrite = new Usuario(idUsuario, email, senha, idNivelUsuario, nome, cpf, endereco, bairro, cidade, uf,
		cep, telefone, foto);
		usuarioWrite.save();
		response.sendRedirect("administracao.jsp");
	}
	
	if (request.getParameter("idAnimal") != null) {
		int idAnimal = Integer.parseInt(request.getParameter("idAnimal"));
		int idProcesso = Integer.parseInt(request.getParameter("idProcesso"));
		String nomeAnimal = request.getParameter("nome");
		String raca = request.getParameter("raca");
		String idade = request.getParameter("idade");
		String porte = request.getParameter("porte");		
		String descricao = request.getParameter("descricao");
		String fotoAnimal = request.getParameter("foto");
		String cor = request.getParameter("cor");
		
		Animal animalWrite = new Animal(idAnimal, idProcesso, nomeAnimal, raca, idade, porte, descricao, cor, fotoAnimal);
		animalWrite.save();
		response.sendRedirect("administracao.jsp");
}%>