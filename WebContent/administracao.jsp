<%@page import="models.Usuario"%>
<%@page import="models.Animal"%>
<%@page import="models.NivelUsuario"%>
<%@page import="models.Processo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="header.jsp" />
<body>
	<jsp:include page="nav.jsp" />

	<meta charset="UTF-8">
	<title>Insert title here</title>
</head>
<body>
	<%
		Usuario usuarioRead = new Usuario();
	String todosUsuarios = usuarioRead.listAll();

	Animal animalRead = new Animal();
	String todosAnimais = animalRead.listAll();

	NivelUsuario nivelUsuarioRead = new NivelUsuario();
	String todosNiveisUsuario = nivelUsuarioRead.listAll();

	Animal processoRead = new Animal();
	String todosParaAdocao = processoRead.listAnimaisProcesso("1");
	String todosEncontrouFamilia = processoRead.listAnimaisProcesso("2");
	String todosVacinado = processoRead.listAnimaisProcesso("3");
	String todosCastrados = processoRead.listAnimaisProcesso("4");
	String todosAdotados = processoRead.listAnimaisProcesso("5");
	%>

<body>


	<!-- Start All Title Box -->
	<div class="all-title-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2>Services</h2>
					<ul class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Principal</a></li>
						<li class="breadcrumb-item active">Administração</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Title Box -->

	<!-- Start Gallery  -->
	<div class="products-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="title-all text-center">
						<h1>ADMINISTRAÇÃO</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="special-menu text-center">
						<div class="button-group filter-button-group">
							<button data-filter=".usuarios">Usuários</button>
							<button data-filter=".animais">Animais</button>
							<button data-filter=".em-adocao">Em Adoção</button>
							<button data-filter=".encontraram-familias">Encontraram
								Fámilias</button>
							<button data-filter=".castrados">Castrados</button>
							<button data-filter=".vacinados">Vacinados</button>
							<button data-filter=".adotados">Adotados</button>

						</div>
					</div>
				</div>
			</div>


			<div class="row special-list table-main">

				<div class="special-grid usuarios" id="grid-tabels">

					<div class="update-box special-grid usuarios">
						<a href="inserir.jsp?tipo=2">
							<button type="button" class="btn btn-primary">Inserir
								Novo Usuário</button>

						</a>
					</div>
					<%=todosUsuarios%>


				</div>

				<div class="special-grid animais" id="grid-tabels">


					<%=todosAnimais%>


				</div>
				<div class="update-box special-grid animais">
					<a href="inserir.jsp?tipo=1">
						<button type="button" class="btn btn-primary" style="margin-left: 50px;">Inserir
							Novo Animal</button>

					</a>
				</div>

				<div class="special-grid em-adocao" id="grid-tabels">

					<%=todosParaAdocao%>

				</div>

				<div class="special-grid encontraram-familias " id="grid-tabels">

					<%=todosEncontrouFamilia%>

				</div>

				<div class="special-grid vacinados" id="grid-tabels">

					<%=todosVacinado%>

				</div>

				<div class="special-grid castrados" id="grid-tabels">

					<%=todosCastrados%>

				</div>

				<div class="special-grid adotados" id="grid-tabels">

					<%=todosAdotados%>


				</div>
			</div>
		</div>
		<!-- End Gallery  -->



		<!-- Start Footer  -->
		<jsp:include page="footer.jsp" />
		<!-- End Footer  -->

		<a href="#" id="back-to-top" title="Back to top"
			style="display: none;">&uarr;</a>

		<!-- ALL JS FILES -->
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- ALL PLUGINS -->
		<script src="js/jquery.superslides.min.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<script src="js/inewsticker.js"></script>
		<script src="js/bootsnav.js."></script>
		<script src="js/images-loded.min.js"></script>
		<script src="js/isotope.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/baguetteBox.min.js"></script>
		<script src="js/form-validator.min.js"></script>
		<script src="js/contact-form-script.js"></script>
		<script src="js/custom.js"></script>
</body>

</html>