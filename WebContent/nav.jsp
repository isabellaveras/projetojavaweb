<%@page import="java.util.Base64"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<jsp:include page="header.jsp" />
<%
	Map<String, String> cookie_list = new HashMap<String, String>();
Cookie[] cookies = request.getCookies();
String administracao ="";
String nomeUser ="";

for (int a = 0; a < cookies.length; a++) {
	cookie_list.put(cookies[a].getName(), cookies[a].getValue());
}

%>
<div class="main-top">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="our-link">
					<ul>
						<%
							if (cookie_list.get("login") == null || cookie_list.get("login") == "") {
						%><li><a href="login.jsp?erroLogin=0"><i
								class="fa fa-user s_color"></i> Entrar</a></li>
						<%
							} else {

						byte[] nomeBytes = Base64.getDecoder().decode(cookie_list.get("userName"));
						nomeUser = new String(nomeBytes);
						 administracao = cookie_list.get("idNivelUsuario");
						%>
						<li><a href="my-account.jsp"><i
								class="fa fa-user s_color"></i> <%=nomeUser%></a></li>
						<li><a href="login.jsp?erroLogin=3"><i
								class="fa fa-user s_color"></i> Sair</a></li>
						<%
							}
						%>
						<li><a href="contact-us.jsp"><i class="fas fa-headset"></i>
								SAC</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Main Top -->

<!-- Start Main Top -->
<nav class="main-nav">
	<!-- Start Navigation -->
	<nav
		class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
		<div class="container">
			<!-- Start nav Navigation -->
			<div class="navbar-nav">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbar-menu" aria-controls="navbars-rs-food"
					aria-expanded="false" aria-label="Toggle navigation">
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="index.jsp"><img
					src="images/logo.png" class="logo" alt="" style="width: 100px;"></a>
			</div>
			<!-- End nav Navigation -->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-menu">
				<ul class="nav navbar-nav ml-auto" data-in="fadeInDown"
					data-out="fadeOutUp">
					<li class="nav-item active"><a class="nav-link"
						href="index.jsp"> <%out.println("Principal");%>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="about.jsp">
							<%
								out.println("Sobre n�s");
							%>
					</a></li>
					
					<%
					if(administracao.equals("1")){  %>
					<li class="nav-item"><a class="nav-link"
						href="administracao.jsp"> <%out.println("Adiministra��o"); %>
					</a></li>
					<%} %>
					<li class="nav-item"><a class="nav-link" href="gallery.jsp">Para ado��o</a></li>
					<li class="dropdown"><a href="#"
						class="nav-link dropdown-toggle " data-toggle="dropdown"> <%out.println("Ado��o"); %>
					</a>
						<ul class="dropdown-menu">
							<li><a href="shop.jsp"> <% 	out.println("Suas ado��es"); %>
							</a></li>
							<li><a href="shop-detail.jsp">Sobre</a></li>
							<li><a href="cart.jsp"> <% 	out.println("Cora��o"); %>
							</a></li>
							<li><a href="checkout.jsp">Checkout</a></li>
							<li><a href="my-account.jsp">My Account</a></li>
							<li><a href="wishlist.jsp">Wishlist</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->

			<!-- Start Atribute Navigation -->
			<div class="attr-nav">
				<ul>
					<li class="side-menu"><a href="cart.jsp"> <i
							class="fa fa-heart"></i> 
							<p>Cora��o</p>
					</a></li>
				</ul>
			</div>
			<!-- End Atribute Navigation -->
		</div>
	</nav>
</nav>